# Contributor: lauren n. liberda <lauren@selfisekai.rocks>
# Maintainer: lauren n. liberda <lauren@selfisekai.rocks>
pkgname=dart-sass
pkgver=1.76.0
pkgrel=0
pkgdesc="The primary implementation of Sass"
url="https://sass-lang.com/dart-sass"
# armv7: no buf
arch="aarch64 x86_64"	# dart
license="MIT"
depends="dartaotruntime"
makedepends="
	buf
	dart-sdk
	"
_protocol_ver=2.7.0
source="
	https://github.com/sass/dart-sass/archive/refs/tags/$pkgver/dart-sass-$pkgver.tar.gz
	https://github.com/sass/sass/archive/refs/tags/embedded-protocol-$_protocol_ver.tar.gz
	sass

	lock.patch
	"
builddir="$srcdir/dart-sass-$pkgver/"

prepare() {
	mkdir build
	mv "$srcdir/sass-embedded-protocol-$_protocol_ver" build/language

	default_prepare

	dart pub get --enforce-lockfile
}

build() {
	UPDATE_SASS_SASS_REPO=false \
		dart run grinder protobuf
	dart compile aot-snapshot -Dversion="$pkgver" ./bin/sass.dart
}

check() {
	# sanity
	dartaotruntime ./bin/sass.aot --version
}

package() {
	install -Dm644 ./bin/sass.aot "$pkgdir"/usr/lib/dart-sass/sass.aot
	install -Dm755 "$srcdir"/sass "$pkgdir"/usr/bin/sass
}

sha512sums="
0a3bf9fa2f58f2fc2eeaa4c56c93971af761cec8c914579d50d916a3da0af7387cd489779685b94ba59c2004b1000db0f6dac9aa4bef67e6814279f4c8865aae  dart-sass-1.76.0.tar.gz
b6de0f1e716c82c33b4919dc9e6b78e2b4ec7e65204400db189c3c52aeae89b38e1de58e26824b2767c316951bb0989aa4d87593a943f0869ef6f4bcc7644a70  embedded-protocol-2.7.0.tar.gz
e974b32f3ca9155868282c2259f693b49bd8c8b57772126efa6e29efedcf4acdb2b1359ae0cb5b0552dfd84daa0ae9273ebe90de7f66670a0a6abd75958cac6d  sass
2c1aebfe586fc8388ab176daadd2ce245a41ef45e1eeade8f8f6438115ba3933a77b8da460eaeb7a31c2e4c32524bb9a7c1dacbbdb188ecde3d11c0c27bada29  lock.patch
"
